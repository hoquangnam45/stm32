#include<SPI.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV8);

  pinMode(SS, OUTPUT);
  //digitalWrite(SS, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(SS, LOW);
  Serial.print("Send: ");
  for (const char *buf = "Hello world from ESP8266\n"; char c = *buf; buf++) {
    SPI.transfer(c);
    Serial.write(c);
  }
  digitalWrite(SS, HIGH);
  delay(1000);
}