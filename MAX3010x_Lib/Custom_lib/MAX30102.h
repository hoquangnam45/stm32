#ifndef __MAX3010X_H
#define __MAX3010X_H

/*************************************************** 
 This is a library written for the Maxim MAX30105 Optical Smoke Detector
 It should also work with the MAX30102. However, the MAX30102 does not have a Green LED.

 These sensors use I2C to communicate, as well as a single (optional)
 interrupt line that is not currently supported in this driver.
 
 Written by Peter Jansen and Nathan Seidle (SparkFun)
 BSD license, all text above must be included in any redistribution.
 *****************************************************/

// This library is written based on SparkFun MAX30105 library
// Intent to run on MAX30102 sensor

#include <stdint.h>
#include <stdbool.h>
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_i2c.h>
#include <string.h>
#include <stm32f1xx_hal_uart.h>
#define byte uint8_t

#define MAX30102_ADDRESS          0x57 //7-bit I2C Address

#define HAL_I2C_TIMEOUT 250 // Default time-out for polling-mode i2c
#define I2C_BUFFER_LENGTH 32 


/**********Process interrupt on STM32**********/
void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c);
/**********************************************/

bool MAX30102_begin(I2C_HandleTypeDef *hi2c); // You should set up i2c configuration struct before execute this

uint32_t MAX30102_getRed(I2C_HandleTypeDef *hi2c); //Returns immediate red value
uint32_t MAX30102_getIR(I2C_HandleTypeDef *hi2c); //Returns immediate IR value
bool MAX30102_getRedIR(I2C_HandleTypeDef *hi2c, uint32_t led_vector[2]);
uint32_t MAX30102_getGreen(I2C_HandleTypeDef *hi2c); //Returns immediate green value
bool MAX30102_safeCheck(I2C_HandleTypeDef *hi2c, uint8_t maxTimeToCheck); //Given a max amount of time, check for new data

// Configuration
void MAX30102_softReset(I2C_HandleTypeDef *hi2c);
void MAX30102_shutDown(I2C_HandleTypeDef *hi2c); 
void MAX30102_wakeUp(I2C_HandleTypeDef *hi2c); 

void MAX30102_setLEDMode(I2C_HandleTypeDef *hi2c, uint8_t mode);

void MAX30102_setADCRange(I2C_HandleTypeDef *hi2c, uint8_t adcRange);
void MAX30102_setSampleRate(I2C_HandleTypeDef *hi2c, uint8_t sampleRate);
void MAX30102_setPulseWidth(I2C_HandleTypeDef *hi2c, uint8_t pulseWidth);

void MAX30102_setPulseAmplitudeRed(I2C_HandleTypeDef *hi2c, uint8_t value);
void MAX30102_setPulseAmplitudeIR(I2C_HandleTypeDef *hi2c, uint8_t value);
void MAX30102_setPulseAmplitudeGreen(I2C_HandleTypeDef *hi2c, uint8_t value);
void MAX30102_setPulseAmplitudeProximity(I2C_HandleTypeDef *hi2c, uint8_t value);

void MAX30102_setProximityThreshold(I2C_HandleTypeDef *hi2c, uint8_t threshMSB);

//Multi-led configuration mode (page 22)
void MAX30102_enableSlot(I2C_HandleTypeDef *hi2c, uint8_t slotNumber, uint8_t device); //Given slot number, assign a device to slot
void MAX30102_disableSlots(I2C_HandleTypeDef *hi2c);

// Data Collection
//Interrupts (page 13, 14)
uint8_t MAX30102_getINT1(I2C_HandleTypeDef *hi2c); //Returns the main interrupt group
uint8_t MAX30102_getINT2(I2C_HandleTypeDef *hi2c); //Returns the temp ready interrupt
void MAX30102_enableAFULL(I2C_HandleTypeDef *hi2c); //Enable/disable individual interrupts
void MAX30102_disableAFULL(I2C_HandleTypeDef *hi2c);
void MAX30102_enableDATARDY(I2C_HandleTypeDef *hi2c);
void MAX30102_disableDATARDY(I2C_HandleTypeDef *hi2c);
void MAX30102_enableALCOVF(I2C_HandleTypeDef *hi2c);
void MAX30102_disableALCOVF(I2C_HandleTypeDef *hi2c);
void MAX30102_enablePROXINT(I2C_HandleTypeDef *hi2c);
void MAX30102_disablePROXINT(I2C_HandleTypeDef *hi2c);
void MAX30102_enableDIETEMPRDY(I2C_HandleTypeDef *hi2c);
void MAX30102_disableDIETEMPRDY(I2C_HandleTypeDef *hi2c);

//FIFO Configuration (page 18)
void MAX30102_setFIFOAverage(I2C_HandleTypeDef *hi2c, uint8_t samples);
void MAX30102_enableFIFORollover(I2C_HandleTypeDef *hi2c);
void MAX30102_disableFIFORollover(I2C_HandleTypeDef *hi2c);
void MAX30102_setFIFOAlmostFull(I2C_HandleTypeDef *hi2c, uint8_t samples);
  
//FIFO Reading
uint16_t MAX30102_check(I2C_HandleTypeDef *hi2c); //Checks for new data and fills FIFO
uint8_t MAX30102_available(void); //Tells caller how many new samples are available (head - tail)
void MAX30102_nextSample(void); //Advances the tail of the sense array
uint32_t MAX30102_getFIFORed(void); //Returns the FIFO sample pointed to by tail
uint32_t MAX30102_getFIFOIR(void); //Returns the FIFO sample pointed to by tail
uint32_t MAX30102_getFIFOGreen(void); //Returns the FIFO sample pointed to by tail

uint8_t MAX30102_getWritePointer(I2C_HandleTypeDef *hi2c);
uint8_t MAX30102_getReadPointer(I2C_HandleTypeDef *hi2c);
void MAX30102_clearFIFO(I2C_HandleTypeDef *hi2c); //Sets the read/write pointers to zero

//Proximity Mode Interrupt Threshold
void MAX30102_setPROXINTTHRESH(I2C_HandleTypeDef *hi2c, uint8_t val);

// Die Temperature
float MAX30102_readTemperature(I2C_HandleTypeDef *hi2c);
//float readTemperatureF();

// Detecting ID/Revision
uint8_t MAX30102_readRevisionID(I2C_HandleTypeDef *hi2c);
uint8_t MAX30102_readPartID(I2C_HandleTypeDef *hi2c);  

// Setup the IC with user selectable settings
// Example usage: MAX30102_setup(hi2cx, 0x1F, 4, 3, 400, 411, 4096)
void MAX30102_setup(I2C_HandleTypeDef *hi2c, byte powerLevel, byte sampleAverage, byte ledMode, uint16_t sampleRate, uint16_t pulseWidth, uint16_t adcRange);

// Low-level I2C communication
uint8_t MAX30102_readRegister8(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint16_t TimeOut);
//uint8_t MAX30102_readRegister8(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg);
void MAX30102_writeRegister8(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t value, uint16_t Timeout);
//void MAX30102_writeRegister8(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t value);

// private:
//  TwoWire *_i2cPort; //The generic connection to user's chosen I2C hardware
//  uint8_t _i2caddr;

  //activeLEDs is the number of channels turned on, and can be 1 to 3. 2 is common for Red+IR.
  
//uint8_t revisionID; 

//void readRevisionID();

void MAX30102_bitMask(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t mask, uint8_t thing);


#define STORAGE_SIZE 4 //Each long is 4 bytes so limit this to fit on your micro
typedef struct Record{
	uint32_t red[STORAGE_SIZE];
  uint32_t IR[STORAGE_SIZE];
  uint32_t green[STORAGE_SIZE];
  byte head;
  byte tail;
} sense_struct; //This is our circular buffer of readings from the sensor



#endif