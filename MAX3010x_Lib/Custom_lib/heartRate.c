/*
 Optical Heart Rate Detection (PBA Algorithm)
 By: Nathan Seidle
 SparkFun Electronics
 Date: October 2nd, 2016
 
 Given a series of IR samples from the MAX30105 we discern when a heart beat is occurring
 Let's have a brief chat about what this code does. We're going to try to detect
 heart-rate optically. This is tricky and prone to give false readings. We really don't
 want to get anyone hurt so use this code only as an example of how to process optical
 data. Build fun stuff with our MAX30105 breakout board but don't use it for actual
 medical diagnosis.
 Excellent background on optical heart rate detection:
 http://www.ti.com/lit/an/slaa655/slaa655.pdf
 Good reading:
 http://www.techforfuture.nl/fjc_documents/mitrabaratchi-measuringheartratewithopticalsensor.pdf
 https://fruct.org/publications/fruct13/files/Lau.pdf
 This is an implementation of Maxim's PBA (Penpheral Beat Amplitude) algorithm. It's been 
 converted to work within the Arduino framework.
*/

/* Copyright (C) 2016 Maxim Integrated Products, Inc., All Rights Reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
* OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of Maxim Integrated
* Products, Inc. shall not be used except as stated in the Maxim Integrated
* Products, Inc. Branding Policy.
*
* The mere transfer of this software does not imply any licenses
* of trade secrets, proprietary technology, copyrights, patents,
* trademarks, maskwork rights, or any other form of intellectual
* property whatsoever. Maxim Integrated Products, Inc. retains all
* ownership rights.
* 
*/

#include "heartRate.h"
#include <math.h>
//int16_t IR_AC_Max = 20;
//int16_t IR_AC_Min = -20;

//int16_t IR_AC_Signal_Current = 0;
//int16_t IR_AC_Signal_Previous;
//int16_t IR_AC_Signal_min = 0;
//int16_t IR_AC_Signal_max = 0;
//int16_t IR_Average_Estimated;

//int16_t positiveEdge = 0;
//int16_t negativeEdge = 0;
//int32_t ir_avg_reg = 0;

//int16_t cbuf[32];
//uint8_t offset = 0;

//  Heart Rate Monitor functions takes a sample value and the sample number
//  Returns true if a beat is detected
//  A running average of four samples is recommended for display on the screen.


//**************50hz Sample rate *************/
//1.5Hz
//static const int32_t FIRCoeffs[12] = {172L, 321L, 579L, 927L, 1360L, 1858L, 2390L, 2916L, 3391L, 3768L, 4012L, 4096L};//the original

//2Hz
//static const int32_t FIRCoeffs[12] = {-111L, -83L, -10L, 202L, 603L, 1212L, 2009L, 2923L, 3842L, 4632L, 5167L, 5356L};//the original

//3Hz
//static const int32_t FIRCoeffs[12] = {-150L, -265L, -376L, -360L, -96L, 515L, 1502L, 2794L, 4210L, 5502L, 6409L, 6735L};//higher bw

/****************25hz Sample rate*****************/
//3Hz
static const int32_t FIRCoeffs[12] = {-112L, -28L, 300L, 692L, 613L, -287L, -1511L, -1696L, 386L, 4518L, 8727L, 10508L};

void init_signal_state(signal_state* led){
	led->raw_signal = 0;
	
	memset(led->signal_raw_buf, 0, BUF_SIZE * sizeof(int32_t));
	memset(led->signal_moving_average_buf, 0, BUF_SIZE * sizeof(int32_t));
	led->signal_buf_idx = 0;
	
	memset(led->fir_unfiltered_signal_buf, 0, 32 * sizeof(int32_t));
	led->fir_unfiltered_buf_idx = 0;
	
	led->maybe_peak = 0;
	led->maybe_valley = 0;
	led->peak_detect = 0;
	led->valley_detect = 0;
	led->valley_min = 0;
	led->peak_max = 0;
	led->count_monotonic = 0;
	led->fir_filtered_signal_pre_1 = 0;
	led->fir_filtered_signal_pre_2 = 0;
}

void processSignal(signal_state* led){
	
	// Shift previous result
	led->fir_filtered_signal_pre_2 = led->fir_filtered_signal_pre_1;
	led->fir_filtered_signal_pre_1 = led->fir_filtered_signal;
	
	// First remove DC 
	led->moving_average_estimate = 0;
	led->signal_raw_buf[led->signal_buf_idx] = led->raw_signal;
	led->signal_moving_average_buf[led->signal_buf_idx] = 0;
	
	for (int i = 0; i < BUF_SIZE; i++)
		led->signal_moving_average_buf[led->signal_buf_idx] += led->signal_raw_buf[i]; // Accumulate raw input
	led->signal_moving_average_buf[led->signal_buf_idx] = led->signal_moving_average_buf[led->signal_buf_idx] >> NUM_BIT_BUF_SIZE; // Taking average
	
	for (int i = 0; i < BUF_SIZE; i++)
		led->moving_average_estimate += led->signal_moving_average_buf[i]; // Accumulate moving average
	led->moving_average_estimate = led->moving_average_estimate >> NUM_BIT_BUF_SIZE; // Taking average
										
	led->fir_unfiltered_signal = led->signal_raw_buf[(led->signal_buf_idx + 1) % BUF_SIZE] - led->moving_average_estimate; // << 2;// Remove average_estimate with delay input and then scale it to amplify signal
										
	led->signal_buf_idx++;
	led->signal_buf_idx %= BUF_SIZE; // Wrap around
	
	// Then low pass FIR filter signal
	led->fir_filtered_signal = 0;
	led->fir_unfiltered_signal_buf[led->fir_unfiltered_buf_idx] = led->fir_unfiltered_signal;
	led->fir_filtered_signal += FIRCoeffs[11] * led->fir_unfiltered_signal_buf[(led->fir_unfiltered_buf_idx - 11) & 0x1F];
	
	for (uint8_t i = 0; i < 11; i++){
		led->fir_filtered_signal += FIRCoeffs[i] * (led->fir_unfiltered_signal_buf[(led->fir_unfiltered_buf_idx - i) & 0x1F] \
															+ led->fir_unfiltered_signal_buf[(led->fir_unfiltered_buf_idx - 22 + i) & 0x1F]);
	}
	led->fir_filtered_signal = led->fir_filtered_signal >> 15; // Scale it down
	
	led->fir_unfiltered_buf_idx++;
	led->fir_unfiltered_buf_idx %= 32; // Wrap around
	
	// Fill in peak-valley info
	if (led->peak_detect == 1){
		led->maybe_peak = 0;
		led->maybe_valley = 0;
		led->peak_detect = 0;
		led->valley_detect = 0;
		led->count_monotonic = 0;
	}
	
	//dot-flat-up
	if ((led->fir_filtered_signal_pre_2  == led->fir_filtered_signal_pre_1) && \
			(led->fir_filtered_signal_pre_1 < led->fir_filtered_signal)) {
		if (led->maybe_valley==1) {
			led->valley_detect = 1;
			led->valley_min = led->fir_filtered_signal_pre_1;
			led->maybe_valley = 0;
		}	
		if (led->maybe_peak==1) {
			led->maybe_peak = 0;
		}
  }
  
	//dot-flat-down
  if ((led->fir_filtered_signal_pre_2  == led->fir_filtered_signal_pre_1) && \
			(led->fir_filtered_signal_pre_1 > led->fir_filtered_signal)) {
		if (led->maybe_valley ==1) {
			led->maybe_valley = 0;
		}	
		if ((led->maybe_peak==1) && (led->fir_filtered_signal_pre_1 > 0)) {
			led->peak_detect = 1;
			led->peak_max = led->fir_filtered_signal_pre_1;
			led->maybe_peak = 0;
		}
  }
  
  //dot-up-flat
  if ((led->fir_filtered_signal_pre_2 < led->fir_filtered_signal_pre_1) && \
			(led->fir_filtered_signal_pre_1 == led->fir_filtered_signal)) {
		led->maybe_peak = 1;
  }
  
	//dot-down-flat
  if ((led->fir_filtered_signal_pre_2 > led->fir_filtered_signal_pre_1) && \
			(led->fir_filtered_signal_pre_1 == led->fir_filtered_signal)) {
		led->maybe_valley = 1;
  }
  //dot-up-down
  if ((led->fir_filtered_signal_pre_2  < led->fir_filtered_signal_pre_1) && \
			(led->fir_filtered_signal_pre_1 > led->fir_filtered_signal) && (led->fir_filtered_signal_pre_1 > 0)) {
	 led->peak_detect = 1;
	 led->peak_max = led->fir_filtered_signal_pre_1;
  }
  
  //dot-down-up
  if ((led->fir_filtered_signal_pre_2  > led->fir_filtered_signal_pre_1) && \
			(led->fir_filtered_signal_pre_1 < led->fir_filtered_signal)) {
	 led->valley_detect = 1;
	 led->valley_min = led->fir_filtered_signal_pre_1;
  }
  
  //  special: detect monotonic (five-in-a-row going up)
  if (led->count_monotonic < 6) {
		if (led->fir_filtered_signal_pre_1 <= led->fir_filtered_signal) {
			 led->count_monotonic++;
		} // detect fiar
		else {
			if (led->count_monotonic > 0){
				 led->count_monotonic = 0;
			}
		}
  }
}

bool checkForBeat(signal_state* ir_led) {
	uint8_t beatDetected = 0;
  //  if detected peak
  if (ir_led->peak_detect == 1) {
		// check whether its a valid beat
		if((ir_led->valley_detect ==1) &&\
			( ir_led->count_monotonic == 6) && \
			((ir_led->peak_max -  ir_led->valley_min) > 75L) && ((ir_led->peak_max - ir_led->valley_min) < 2500000L)) {
      //Heart beat!!!
      beatDetected = 1;
		}
	}
 return(beatDetected);
}

double calc_spo2(signal_state* red_led, signal_state* ir_led){
	return 0;
}
//bool checkForBeat(uint32_t* raw_buf, uint8_t buf_size) {
//  bool beatDetected = false;

//  //  Save current state
//  IR_AC_Signal_Previous = IR_AC_Signal_Current;
//  
//  //This is good to view for debugging
//  //Serial.print("Signal_Current: ");
//  //Serial.println(IR_AC_Signal_Current);

//  //  Process next data sample
//  IR_Average_Estimated = averageDCEstimator(&ir_avg_reg, sample);
//  IR_AC_Signal_Current = lowPassFIRFilter(sample - IR_Average_Estimated);

//  //  Detect positive zero crossing (rising edge)
//  if ((IR_AC_Signal_Previous < 0) & (IR_AC_Signal_Current >= 0))
//  {
//  
//    IR_AC_Max = IR_AC_Signal_max; //Adjust our AC max and min
//    IR_AC_Min = IR_AC_Signal_min;

//    positiveEdge = 1;
//    negativeEdge = 0;
//    IR_AC_Signal_max = 0;

//    //if ((IR_AC_Max - IR_AC_Min) > 100 & (IR_AC_Max - IR_AC_Min) < 1000)
//    if ((IR_AC_Max - IR_AC_Min) > 20 & (IR_AC_Max - IR_AC_Min) < 1000)
//    {
//      //Heart beat!!!
//      beatDetected = true;
//    }
//  }

//  //  Detect negative zero crossing (falling edge)
//  if ((IR_AC_Signal_Previous > 0) & (IR_AC_Signal_Current <= 0))
//  {
//    positiveEdge = 0;
//    negativeEdge = 1;
//    IR_AC_Signal_min = 0;
//  }

//  //  Find Maximum value in positive cycle
//  if (positiveEdge & (IR_AC_Signal_Current > IR_AC_Signal_Previous))
//  {
//    IR_AC_Signal_max = IR_AC_Signal_Current;
//  }

//  //  Find Minimum value in negative cycle
//  if (negativeEdge & (IR_AC_Signal_Current < IR_AC_Signal_Previous))
//  {
//    IR_AC_Signal_min = IR_AC_Signal_Current;
//  }
//  
//  return(beatDetected);
//}

////  Average DC Estimator
//int16_t averageDCEstimator(int32_t *p, uint16_t x) {
//  *p += ((((long) x << 15) - *p) >> 4);
//  return (*p >> 15);
//}

////  Low Pass FIR Filter
//int16_t lowPassFIRFilter(int16_t din) {  
//  cbuf[offset] = din;

//  int32_t z = mul16(FIRCoeffs[11], cbuf[(offset - 11) & 0x1F]);
//  
//  for (uint8_t i = 0 ; i < 11 ; i++)
//  {
//    z += mul16(FIRCoeffs[i], cbuf[(offset - i) & 0x1F] + cbuf[(offset - 22 + i) & 0x1F]);
//  }

//  offset++;
//  offset %= 32; //Wrap condition

//  return(z >> 15);
//}

////  Integer multiplier
//int32_t mul16(int16_t x, int16_t y) {
//  return((long)x * (long)y);
//}


