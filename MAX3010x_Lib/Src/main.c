#include "main.h"
#include "MAX30102.h"
#include "heartRate.h"
//#include "spo2_algorithm.h"
//#include <stdio.h>
#include "alternate_printf.h"
#include <math.h>
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);

//#ifdef __GNUC__
//		#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
//#else
//		#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
//#endif

//PUTCHAR_PROTOTYPE {
//		HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
//		return ch;
//}

//#define MAX_BRIGHTNESS 255
//uint32_t irBuffer[100]; //infrared LED sensor data
//uint32_t redBuffer[100];  //red LED sensor data
//int32_t bufferLength; //data length
//int32_t spo2; //SPO2 value
//int8_t validSPO2; //indicator to show if the SPO2 calculation is valid
//int32_t heartRate; //heart rate value
//int8_t validHeartRate; //indicator to show if the heart rate calculation is valid
//byte pulseLED = 11; //Must be on PWM pin
//byte readLED = 13; //Blinks with each data read

//int main(void){
//  HAL_Init();
//  SystemClock_Config();
//  MX_GPIO_Init();
//  MX_I2C1_Init();
//  MX_USART1_UART_Init();
//	
//	if (!MAX30102_begin(&hi2c1))
//		while(1);
//	
//	printf("Attach sensor to finger with rubber band. Press any key to start conversion\n");
//  char dummy = 0;
//	HAL_UART_Receive_IT(&huart1, (uint8_t*) &dummy, 1);
//	while (!dummy); //wait until user presses a key
//	byte ledBrightness = 60; //Options: 0=Off to 255=50mA
//  byte sampleAverage = 4; //Options: 1, 2, 4, 8, 16, 32
//  byte ledMode = 2; //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
//  byte sampleRate = 100; //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
//  int pulseWidth = 411; //Options: 69, 118, 215, 411
//  int adcRange = 4096; //Options: 2048, 4096, 8192, 16384
//	MAX30102_setup(&hi2c1, ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);

//  while (1){
//		bufferLength = 100; //buffer length of 100 stores 4 seconds of samples running at 25sps
//		//read the first 100 samples, and determine the signal range
//		for (byte i = 0 ; i < bufferLength ; i++){
//			while (MAX30102_available() == false) //do we have new data?
//				MAX30102_check(&hi2c1); //Check the sensor for new data

//			redBuffer[i] = MAX30102_getRed(&hi2c1);
//			irBuffer[i] = MAX30102_getIR(&hi2c1);
//			MAX30102_nextSample(); //We're finished with this sample so move to next sample

//			printf("red=%d, ir=%d\r\n", redBuffer[i], irBuffer[i]);
//		}

//		//calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
//		maxim_heart_rate_and_oxygen_saturation32(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);

//		//Continuously taking samples from MAX30102.  Heart rate and SpO2 are calculated every 1 second
//		while (1){
//			//dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
//			for (byte i = 25; i < 100; i++){
//				redBuffer[i - 25] = redBuffer[i];
//				irBuffer[i - 25] = irBuffer[i];
//			}

//			//take 25 sets of samples before calculating the heart rate.
//			for (byte i = 75; i < 100; i++)
//			{
//				while (MAX30102_available() == false) //do we have new data?
//					MAX30102_check(&hi2c1); //Check the sensor for new data

//				redBuffer[i] = MAX30102_getRed(&hi2c1);
//				irBuffer[i] = MAX30102_getIR(&hi2c1);
//				MAX30102_nextSample(); //We're finished with this sample so move to next sample

//				//send samples and calculation result to terminal program through UART
//				printf("red=%d, ir=%d, HR=%d, HRvalid=%d, SPO2=%d, SPO2Valid= %d\r\n", 
//					redBuffer[i],\
//					irBuffer[i],\
//					heartRate,\
//					validHeartRate,\
//					spo2,\
//					validSPO2
//				);
//			}

//			//After gathering 25 new samples recalculate HR and SP02
//			maxim_heart_rate_and_oxygen_saturation32(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
//			//HAL_Delay(1000);
//		}
//  }
//}


//const byte RATE_SIZE = 1; //Increase this for more averaging. 4 is good.
//byte rates[RATE_SIZE]; //Array of heart rates
//byte rateSpot = 0;
//long lastBeat = 0; //Time at which the last beat occurred
//float beatsPerMinute;
//int beatAvg;

//int main(){
//	HAL_Init();
//  SystemClock_Config();
//  MX_GPIO_Init();
//  MX_I2C1_Init();
//  MX_USART1_UART_Init();
//	
//	if (!MAX30102_begin(&hi2c1))
//		while(1);
//	
//	//printf("Press any keyboard to start\r\n");
//  //char dummy = 0;
//	//HAL_UART_Receive_IT(&huart1, (uint8_t*) &dummy, 1);
//	//while (!dummy); //wait until user presses a key
//	byte ledBrightness = 0x3F;//60; //Options: 0=Off to 255=50mA
//  byte sampleAverage = 8; //Options: 1, 2, 4, 8, 16, 32
//  byte ledMode = 2; //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
//  byte sampleRate = 400;//100; //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
//  int pulseWidth = 411; //Options: 69, 118, 215, 411
//  int adcRange = 4096; //Options: 2048, 4096, 8192, 16384
//	MAX30102_setup(&hi2c1, ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);
//	MAX30102_setPulseAmplitudeRed(&hi2c1, 0x0A); //Turn Red LED to low to indicate sensor is running
//	MAX30102_setPulseAmplitudeGreen(&hi2c1, 0);
//	while (1){
//		long irValue = MAX30102_getIR(&hi2c1);
//		printf("%ld\r\n", irValue);
//		
//		/*
//		long irValue = MAX30102_getIR(&hi2c1);
//		if (checkForBeat(irValue) == true){
//			//We sensed a beat!
//			long delta = HAL_GetTick() - lastBeat;
//			lastBeat = HAL_GetTick();
//			printf("%f\n", delta / 1000.0);
//			beatsPerMinute = 60. / (delta / 1000.0);

//			if (beatsPerMinute < 255 && beatsPerMinute > 20){
//				rates[rateSpot++] = (byte)beatsPerMinute; //Store this reading in the array
//				rateSpot %= RATE_SIZE; //Wrap variable

//				//Take average of readings
//				beatAvg = 0;
//				for (byte x = 0 ; x < RATE_SIZE ; x++)
//					beatAvg += rates[x];
//				beatAvg /= RATE_SIZE;
//			}
//		}

//		printf("IR=%ld, BPM =%f, Avg BPM=%d\r\n", irValue, beatsPerMinute, beatAvg);

//		if (irValue < 50000)
//			printf(" No finger?\r\n");
//		*/
//	}
//	return 0;
//}

//#define MAX_BRIGHTNESS 255
//uint32_t irBuffer[100]; //infrared LED sensor data
//uint32_t redBuffer[100];  //red LED sensor data
//int32_t bufferLength; //data length
//int32_t spo2; //SPO2 value
//int8_t validSPO2; //indicator to show if the SPO2 calculation is valid
//int32_t heartRate; //heart rate value
//int8_t validHeartRate; //indicator to show if the heart rate calculation is valid
//byte pulseLED = 11; //Must be on PWM pin
//byte readLED = 13; //Blinks with each data read


// Main process sensor data 50hz
//#define ledBrightness 0x3F
//#define sampleAverage 8
//#define ledMode 2
//#define sampleRate 400
//#define pulseWidth 411
//#define adcRange 4096
//#define BPM_RESET_TIME 2000
//static char debug_buf[255];
//static signal_state red_led, ir_led;
//static uint32_t led_vector[2];
//static uint32_t old_heartbeat_timestamp;
//static uint8_t beat_per_minute;
//static float bpm_temp;
//static uint8_t beatDetected = 0;

//int main(void){
//  HAL_Init();
//  SystemClock_Config();
//  MX_GPIO_Init();
//  MX_I2C1_Init();
//  MX_USART1_UART_Init();
//	
//	HAL_Delay(100);
//	
//	if (!MAX30102_begin(&hi2c1)){
//		while(1);//{printf("test");}
//	}
//	//while(1){printf("test2");}
//	//printf("Attach sensor to finger with rubber band. Press any key to start conversion\n");
//  //char dummy = 0;
//	//HAL_UART_Receive_IT(&huart1, (uint8_t*) &dummy, 1);
//	//while (!dummy); //wait until user presses a key
//	
//	MAX30102_setup(&hi2c1, ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);
//	
//  init_signal_state(&red_led);
//	init_signal_state(&ir_led);
//	
//	int i = 0;
//	int avg_exec_time_ms = 0;
//	int avg_uart_time_ms = 0;
//	while (1){
//		if(MAX30102_getRedIR(&hi2c1, led_vector)){
//			uint32_t timestamp = HAL_GetTick();
//			
//			red_led.raw_signal = (int32_t) led_vector[0];
//			ir_led.raw_signal = (int32_t) led_vector[1];
//			processSignal(&red_led);
//			processSignal(&ir_led);
//			
//			if (timestamp - old_heartbeat_timestamp >= BPM_RESET_TIME){
//				beat_per_minute = 0;
//			}
//			
//			beatDetected = checkForBeat(&ir_led);
//			if (beatDetected){
//				//printf("hihi\r\n");	
//				bpm_temp = 60000. / (timestamp - old_heartbeat_timestamp);
//				if (bpm_temp < 180 && bpm_temp > 30) beat_per_minute = (uint8_t) bpm_temp;
//				//printf("%f; %d; %d; %d\r\n", bpm_temp, timestamp, old_heartbeat_timestamp, timestamp - old_heartbeat_timestamp);
//				old_heartbeat_timestamp = timestamp;
//				//printf("%f\r\n",bpm_temp);
//				//if (bpm_temp < 255) beat_per_minute = (uint8_t) bpm_temp;
//				
//			}
//			//const uint32_t temp = 1 << 31;
//			//uint32_t timestamp_start_uart = HAL_GetTick();
//			sprintf(debug_buf, "%d:%d:%d:%d:%u:%u:%u\r\n", ir_led.raw_signal,\
//																								ir_led.moving_average_estimate,\
//																								ir_led.fir_unfiltered_signal,\
//																								ir_led.fir_filtered_signal,\
//																								beatDetected,\
//																								beat_per_minute,\
//																								timestamp);
//			HAL_UART_Transmit(&huart1, (uint8_t*) debug_buf, strlen(debug_buf), 20);
//			/*
//			uint32_t timestamp_end = HAL_GetTick();
//			if (i < 1000){
//				avg_uart_time_ms += timestamp_end - timestamp_start_uart;
//				avg_exec_time_ms += timestamp_start_uart - timestamp;
//			}
//			else{
//				avg_uart_time_ms /= 1000;
//				avg_exec_time_ms /= 1000	;
//				printf("Exec time ms: %d - UART time ms - %d\r\n", avg_exec_time_ms, avg_uart_time_ms); 
//				break;
//			}
//			i++;*/
//		}
//  }
//}

// Main process sensor data 25hz
#define ledBrightness 0x3F
#define sampleAverage 16
#define ledMode 2
#define sampleRate 400
#define pulseWidth 411
#define adcRange 4096
#define BPM_RESET_TIME 2000
static char debug_buf[255];
static signal_state red_led, ir_led;
static uint32_t led_vector[2];
static uint32_t old_heartbeat_timestamp;
static uint8_t beat_per_minute;
static float bpm_temp;
static uint8_t beatDetected = 0;
static float ratio_average = 0;
static uint8_t spo2 = 0;
int main(void){
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
	
	HAL_Delay(100);
	
	if (!MAX30102_begin(&hi2c1)){
		while(1);//{printf("test");}
	}
	//while(1){printf("test2");}
	//printf("Attach sensor to finger with rubber band. Press any key to start conversion\n");
  //char dummy = 0;
	//HAL_UART_Receive_IT(&huart1, (uint8_t*) &dummy, 1);
	//while (!dummy); //wait until user presses a key
	
	MAX30102_setup(&hi2c1, ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);
	
  init_signal_state(&red_led);
	init_signal_state(&ir_led);
	
	//int i = 0;
	//int avg_exec_time_ms = 0;
	//int avg_uart_time_ms = 0;
	while (1){
		if(MAX30102_getRedIR(&hi2c1, led_vector)){
			uint32_t timestamp = HAL_GetTick();
			
			red_led.raw_signal = (int32_t) led_vector[0];
			ir_led.raw_signal = (int32_t) led_vector[1];
			processSignal(&red_led);
			processSignal(&ir_led);
			
			if (timestamp - old_heartbeat_timestamp >= BPM_RESET_TIME){
				beat_per_minute = 0;
			}
			
			beatDetected = checkForBeat(&ir_led);
			if (beatDetected){
				//printf("hihi\r\n");	
				bpm_temp = 60000. / (timestamp - old_heartbeat_timestamp);
				if (bpm_temp < 180 && bpm_temp > 30) beat_per_minute = (uint8_t) bpm_temp;
				//printf("%f; %d; %d; %d\r\n", bpm_temp, timestamp, old_heartbeat_timestamp, timestamp - old_heartbeat_timestamp);
				old_heartbeat_timestamp = timestamp;
				//printf("%f\r\n",bpm_temp);
				//if (bpm_temp < 255) beat_per_minute = (uint8_t) bpm_temp;
				
			}
			
			if ((ir_led.peak_max - ir_led.valley_min > 0) && (red_led.moving_average_estimate > 0)){
					ratio_average = (float)(red_led.peak_max - red_led.valley_min) / (ir_led.peak_max - ir_led.valley_min) * ir_led.moving_average_estimate / red_led.moving_average_estimate;
					spo2 = floor(110-25*ratio_average);
					if (spo2 > 100) spo2 = 100;
			}
			else{
					spo2 = 0;
			}
			//printf("%f - spo2:%d\r\n", ratio_average, spo2);
			//const uint32_t temp = 1 << 31;
			//uint32_t timestamp_start_uart = HAL_GetTick();
			sprintf(debug_buf, "%d:%d:%d:%d:%d:%d:%d:%d:%u:%u:%u:%u\r\n", red_led.raw_signal,\
																																	red_led.moving_average_estimate,\
																																	red_led.fir_unfiltered_signal,\
																																	red_led.fir_filtered_signal,\
																																	ir_led.raw_signal,\
																																	ir_led.moving_average_estimate,\
																																	ir_led.fir_unfiltered_signal,\
																																	ir_led.fir_filtered_signal,\
																																	beatDetected,\
																																	beat_per_minute,\
																																	spo2,\
																																	timestamp);
			HAL_UART_Transmit(&huart1, (uint8_t*) debug_buf, strlen(debug_buf), 20);
			/*
			uint32_t timestamp_end = HAL_GetTick();
			if (i < 1000){
				avg_uart_time_ms += timestamp_end - timestamp_start_uart;
				avg_exec_time_ms += timestamp_start_uart - timestamp;
			}
			else{
				avg_uart_time_ms /= 1000;
				avg_exec_time_ms /= 1000	;
				printf("Exec time ms: %d - UART time ms - %d\r\n", avg_exec_time_ms, avg_uart_time_ms); 
				break;
			}
			i++;*/
		}
  }
}

////Get sample
//#define ledBrightness 0x3F
//#define sampleAverage 16
//#define ledMode 2
//#define sampleRate 400
//#define pulseWidth 411
//#define adcRange 4096
//#define BPM_RESET_TIME 2000
//static uint32_t led_vector[2];
//static char debug_buf[255];
//int main(void){
//  HAL_Init();
//  SystemClock_Config();
//  MX_GPIO_Init();
//  MX_I2C1_Init();
//  MX_USART1_UART_Init();
//	
//	HAL_Delay(100);
//	
//	if (!MAX30102_begin(&hi2c1)){
//		while(1);//{printf("test");}
//	}
//	
//	MAX30102_setup(&hi2c1, ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);
//	
//	while (1){
//		if(MAX30102_getRedIR(&hi2c1, led_vector)){
//			uint32_t timestamp = HAL_GetTick();
//			sprintf(debug_buf, "%d;%d;%d\r\n", led_vector[0], led_vector[1], timestamp);
//			HAL_UART_Transmit(&huart1, (uint8_t*) debug_buf, strlen(debug_buf), 20);
//		}
//  }
//}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
