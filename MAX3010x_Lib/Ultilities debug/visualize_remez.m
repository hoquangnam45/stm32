function visualize_remez(b_calc)
    b = [172 321 579 927 1360 1858 2390 2916 3391 3768 4012 4096];
    b_target = [b b(11:-1:1)]./2^15;
    fvtool(b_target,1)
    f = [0 0.0004 0.188 1];
    a = [1 1 0 0];
    %b_calc = remez(22, f, a); % 2hz
%     b_ratio = b_target ./ b_calc;
%     %b_ratio = abs(b_ratio);
%     mean_b_ratio = mean(b_ratio);
%     b_ratio = (b_ratio - mean(b_ratio)) ./ mean(b_ratio);
%     b_calc = round(b_calc .* mean_b_ratio);
%     disp(b_calc)
%     disp(b_target)
%     eval = sumsqr(b_ratio);
%     disp(eval)
    hold on
    for i=1:length(f)-1
        %plot([f(i) f(i+1)] .* 25, [a(i) a(i+1)], 'blue')
        %plot([f(i) f(i+1)] .* 25, 20*log10([a(i) a(i+1)]), 'blue')
        %disp([f(i) f(i+1)] .* 25)
        %disp(20*log10([a(i) a(i+1)] .* 2^15))
    end
    [h, w] = freqz(b_calc, 1, 500, 50);
    [h2, w2] = freqz(b_target, 1, 500, 50);
    %plot(w, abs(h), 'red')
    plot(w, 20*log10(abs(h)), 'red')
    plot(w2, 20*log10(abs(h2)), 'blue')
    hold off
end