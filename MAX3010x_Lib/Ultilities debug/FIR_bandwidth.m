function x_cross = FIR_bandwidth(h, w, passband_lower_ripple)
    magnitude_db = 20*log10(abs(h));
    index = find(magnitude_db < passband_lower_ripple);
    %index = find(magnitude_db < 0);
    if numel(index) == 0 || index(1) == 1
        x_cross = 0;
        return
    end
    y_point_a = 20*log10(abs(h(index(1))));
    y_point_b = 20*log10(abs(h(index(1) - 1)));
    x_point_a = w(index(1));
    x_point_b = w(index(1) - 1);
    x_cross = x_point_b - (x_point_a - x_point_b) / (y_point_a - y_point_b) * (y_point_b - passband_lower_ripple);
end