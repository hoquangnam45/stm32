function find_norm_freq
    b_org = [172 321 579 927 1360 1858 2390 2916 3391 3768 4012 4096];
    b_org = [b b(11:-1:1)];
    
    step_delta = 0.0001;
    min_val = 0;
    i_min = 0;
    j_min = 0;
    init = 0;
    warning('off','all')
    warning
%     b_calc = remez(22,[0 step_delta 0.8455 1], [1 1 0 0]);
%     disp(b_calc)
%     b_ratio = b_target ./ b_calc;
%     disp(b_ratio)
%     %b_ratio = abs(b_ratio);
%     b_ratio = (b_ratio - mean(b_ratio)) ./ mean(b_ratio);
%     eval = sumsqr(b_ratio);
%     disp(eval)
    for i = step_delta:step_delta:1-step_delta*2
        for j = i+step_delta:step_delta:1-step_delta
%             disp('///////////')
%             disp(i)
%             disp(j)
%             break
%             disp('<<<<<<<<<<<')
%             disp(' ')
            b_calc = remez(22,[0 i j 1], [1 1 0 0]);
            if any(isinf(b_calc) == 1) || any((isnan(b_calc) == 1))
                continue
            end
            b_ratio = b_target ./ b_calc;
            %b_ratio = abs(b_ratio);
            mean_p_ratio = mean(b_ratio);
            b_ratio = (b_ratio - mean(b_ratio)) ./ mean(b_ratio);
            eval = sumsqr(b_ratio);
            %disp(eval) q
            %disp(b_ratio)
            if (eval < min_val && eval > 0) || init == 0
                i_min = i;
                j_min = j;
                min_val = eval;
                init = 1;
                disp('>>>>')
                disp(i_min)
                disp(j_min)
                disp(mean_p_ratio)
                disp(eval)
                disp(' ')
            end
%             break
        end
    end
    %disp(i_min)
    %disp(j_min)
end
    