from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
import pandas as pd
import sys 
import serial.tools.list_ports
import msvcrt
import threading
import os
import numpy as np
import csv
import math
import py_animate
import py_plotter

BPM_RESET_TIME = 2000
MAX_LIST_SIZE = 250

red_raw_signal_list = []
red_moving_average_list = []
red_unfilter_signal_list = []
red_filter_signal_list = []

ir_raw_signal_list = []
ir_moving_average_list = []
ir_unfilter_signal_list = []
ir_filter_signal_list = []

ir_beat_list = []
timestamp_list = []

beat_per_minute = [None]
spo2 = [None]

red_signal_list = [None]
ir_signal_list = [None]

def get_data_v1_sim():
    global red_raw_signal_list
    global red_moving_average_list
    global red_unfilter_signal_list
    global red_filter_signal_list

    global ir_raw_signal_list
    global ir_moving_average_list
    global ir_unfilter_signal_list
    global ir_filter_signal_list

    global ir_beat_list
    global timestamp_list

    log_file = './raw_data_sample_rate_25hz_STM32.csv'
    with open(log_file, 'r') as f:
        plots = csv.reader(f, delimiter=';')

        old_beat_timestamp = None
        red_led_state = py_plotter.signal_state()
        ir_led_state = py_plotter.signal_state()
        #idx = 0
        for row in plots:
            # idx += 1
            # print(idx) 
            red_led_state.raw_signal = int(row[0])
            ir_led_state.raw_signal = int(row[1])
            current_timestamp = int(row[2])

            red_led_state.process_signal()
            ir_led_state.process_signal()

            timestamp_list.append(current_timestamp)

            red_raw_signal_list.append(red_led_state.raw_signal)
            ir_raw_signal_list.append(ir_led_state.raw_signal)

            red_moving_average_list.append(red_led_state.moving_average)
            ir_moving_average_list.append(ir_led_state.moving_average)

            red_unfilter_signal_list.append(red_led_state.unfiltered_signal)
            ir_unfilter_signal_list.append(ir_led_state.unfiltered_signal)

            red_filter_signal_list.append(red_led_state.filtered_signal)
            ir_filter_signal_list.append(ir_led_state.filtered_signal)

            ir_beat_list.append(ir_led_state.checkForBeat())

            if len(timestamp_list) > MAX_LIST_SIZE:
                red_raw_signal_list = red_raw_signal_list[1:]
                red_moving_average_list = red_moving_average_list[1:]
                red_unfilter_signal_list = red_unfilter_signal_list[1:]
                red_filter_signal_list = red_filter_signal_list[1:]

                ir_raw_signal_list = ir_raw_signal_list[1:]
                ir_moving_average_list = ir_moving_average_list[1:]
                ir_unfilter_signal_list = ir_unfilter_signal_list[1:]
                ir_filter_signal_list = ir_filter_signal_list[1:]

                ir_beat_list = ir_beat_list[1:]
                timestamp_list = timestamp_list[1:]

            if old_beat_timestamp != None:
                if (current_timestamp - old_beat_timestamp >= BPM_RESET_TIME): beat_per_minute[0] = 0

            if ir_beat_list[-1]:  
                if old_beat_timestamp != None: 
                    beat_per_minute[0] = int(60000. / (current_timestamp - old_beat_timestamp))
                old_beat_timestamp = current_timestamp
            
            if (ir_led_state.peak_max - ir_led_state.valley_min) > 0 and red_led_state.moving_average > 0:
                ratio_average = (red_led_state.peak_max - red_led_state.valley_min) / (ir_led_state.peak_max - ir_led_state.valley_min) * ir_led_state.moving_average / red_led_state.moving_average
                spo2[0] = math.floor(110-25*ratio_average)
                #print(str(spo2[0])+'%')
            else:
                spo2[0] = 999
            
            red_signal_list[:] = [red_raw_signal_list, red_moving_average_list, red_unfilter_signal_list, red_filter_signal_list]
            ir_signal_list[:] = [ir_raw_signal_list, ir_moving_average_list, ir_unfilter_signal_list, ir_filter_signal_list, ir_beat_list]

            # offset += 1
            # offset = offset % ARRAY_BUF_SIZE

            #time.sleep(20e-3) #50hz
            time.sleep(40e-3) #25hz

# def animate(i, ax, timestamp_list, red_signal_list, ir_signal_list, beat_per_minute, spo2):
#     if red_signal_list[0] is None or ir_signal_list[0] is None:
#         return
#     red_raw_signal_list, red_moving_average_list, red_unfilter_signal_list, red_filter_signal_list = red_signal_list
#     ir_raw_signal_list, ir_moving_average_list, ir_unfilter_signal_list, ir_filter_signal_list, ir_beat_list = ir_signal_list

#     #ax[0].clear()
#     #ax[1].clear()
#     for i in range(ax.shape[0]):
#         for j in range(ax.shape[1]):
#             ax[i,j].clear()

#     # Draw
#     line1, = ax[0,0].plot(timestamp_list, red_raw_signal_list, marker='.', color='blue')
#     line2, = ax[0,0].plot(timestamp_list, red_moving_average_list, marker='.', color='red')

#     ax[0,0].set_xlabel('Timestamp: ms')
#     ax[0,0].set_ylabel('Value')
#     ax[0,0].legend(['red raw value', 'red moving average'], loc='lower right')
#     ax[0,0].set_title('Red raw signal and moving average')

#     line3, = ax[0,1].plot(timestamp_list, ir_raw_signal_list, marker='.', color='blue')
#     line4, = ax[0,1].plot(timestamp_list, ir_moving_average_list, marker='.', color='red')

#     ax[0,1].set_xlabel('Timestamp: ms')
#     ax[0,1].set_ylabel('Value')
#     ax[0,1].legend(['ir raw value', 'ir moving average'], loc='lower right')
#     ax[0,1].set_title('Ir raw signal and moving average')



#     line5, = ax[1,0].plot(timestamp_list, red_unfilter_signal_list, marker='.', color='blue')

#     ax[1,0].set_xlabel('Timestamp: ms')
#     ax[1,0].set_ylabel('Value')
#     ax[1,0].legend(['Red filtered DC signal'], loc='lower right')
#     ax[1,0].set_title('Red filtered DC signal')

#     line6, = ax[1,1].plot(timestamp_list, ir_unfilter_signal_list, marker='.', color='blue')
    
#     ax[1,1].set_xlabel('Timestamp: ms')
#     ax[1,1].set_ylabel('Value')
#     ax[1,1].legend(['IR filtered DC signal'], loc='lower right')
#     ax[1,1].set_title('IR filtered DC signal')



#     line7, = ax[2,0].plot(timestamp_list, red_filter_signal_list, marker='.', color='blue')

#     ax[2,0].set_xlabel('Timestamp: ms')
#     ax[2,0].set_ylabel('Value')
#     ax[2,0].legend(['Red filtered FIR+DC signal'], loc='lower right')
#     ax[2,0].set_title('Red filtered FIR+DC signal')

#     line8, = ax[2,1].plot(timestamp_list, ir_filter_signal_list, marker='.', color='blue')
    
#     # Shift beat list left
#     ir_beat_list = ir_beat_list[1:]
#     ir_beat_list.append(0)

#     heartbeat_timestamp = [timestamp for timestamp, beat in zip(timestamp_list, ir_beat_list) if beat == 1]
#     heartbeat_ir_value = [ir_value for ir_value, beat in zip(ir_filter_signal_list, ir_beat_list) if beat == 1]

#     line9, = ax[2,1].plot(heartbeat_timestamp, heartbeat_ir_value, 'ro')

#     ax[2,1].set_xlabel('Timestamp: ms - BPM: ' + str(beat_per_minute[0]) + ' - SPO2: ' + str(spo2[0]) + "%")
#     ax[2,1].set_ylabel('Value')
#     ax[2,1].legend(['IR filtered FIR+DC signal','Heart beat'], loc='lower right')
#     ax[2,1].set_title('IR filtered FIR+DC signal + Heart beat')

#     return line1,line2,line3,line4,line5,line6,line7,line8,line9

def main():
    plot_interval = 40    # Period at which the plot animation updates [ms]

    get_data_thread = threading.Thread(target=get_data_v1_sim, kwargs=dict())
    get_data_thread.start()

    fig, ax = plt.subplots(3,2)

    anim = animation.FuncAnimation(fig, py_animate.animate,\
                                    fargs=(ax, timestamp_list, red_signal_list, ir_signal_list, beat_per_minute, spo2),\
                                    interval=plot_interval)

    plt.show()
    os._exit(0)

if __name__ == '__main__':
    main()