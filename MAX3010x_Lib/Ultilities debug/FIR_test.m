function FIR_test%(Num)
    % Sample rate = 50 or 100hz
    b_org = [172 321 579 927 1360 1858 2390 2916 3391 3768 4012 4096];
    b_org = [b_org b_org(11:-1:1)];
    
    b1 = round(46000*remez(22,[0 0.0079 0.186 1], [1 1 0 0])); %nominal 1.5Hz
    b2 = round(46000*remez(22,[0 0.011 0.24 1], [1 1 0 0])); %2Hz
    b3 = round(46000*remez(22,[0 0.015 0.255 1], [1 1 0 0])); %2.5Hz
    b4 = round(46000*remez(22,[0 0.018 0.27 1], [1 1 0 0])); %3H
    % Sample rate 25 hz
    b5 = [-112 -28 300 692 613 -287 -1511 -1696 386 4518 8727 10508];
    b5 = [b5 b5(11:-1:1)];    
   
    %b_list = {b_org, b1, b2, b3, b4};
    b_list = {b_org};
    b_list2 = {b5};
    
    fs = 100;
    fs2 = 25;
    %fs = 25;
    a = 1;
    
    
    subplot(2,1,1)
    for k=1:length(b_list)
        [h,w] = freqz(b_list{k}, a, 500, fs);
        h = h./(2^15);
        plot(w,20*log10(abs(h)));
        hold on
        b_bandwidth = FIR_bandwidth(h, w, -0.5);
        disp(b_bandwidth);
    end    
    
    for k=1:length(b_list2)
        [h,w] = freqz(b_list2{k}, a, 500, fs2);
        h = h./(2^15);
        plot(w,20*log10(abs(h)));
        hold on
        b_bandwidth = FIR_bandwidth(h, w, -0.5);
        disp(b_bandwidth);
    end
    
    xlabel('Frequency (Hz)');
    ylabel('Magnitude (dB)');
    legend('original', 'imitate')%, '1.5hz', '2hz', '2.5hz', '3hz')
    
    subplot(2,1,2)
    for k=1:length(b_list)
        [h,w] = freqz(b_list{k}, a, 500, fs);
        h = h./(2^15);
        plot(w,360/(2*pi)*angle(h));
        hold on
    end    
    
    for k=1:length(b_list2)
        [h,w] = freqz(b_list2{k}, a, 500, fs2);
        h = h./(2^15);
        plot(w,360/(2*pi)*angle(h));
        hold on
    end
    
    xlabel('Frequency (Hz)');
    ylabel('Phase (rad)');
    legend('original', 'imitate')%, '1.5hz', '2hz', '2.5hz', '3hz')
    hold off
end