from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
import pandas as pd
import sys 
import serial.tools.list_ports
import msvcrt
import threading
import os
import numpy as np
import py_plotter
import math 

BPM_RESET_TIME = 2000
MAX_LIST_SIZE = 250

red_raw_signal_list = []
red_moving_average_list = []
red_unfilter_signal_list = []
red_filter_signal_list = []

ir_raw_signal_list = []
ir_moving_average_list = []
ir_unfilter_signal_list = []
ir_filter_signal_list = []

ir_beat_list = []
timestamp_list = []

beat_per_minute = [None]
spo2 = [None]

red_signal_list = [None]
ir_signal_list = [None]

def get_data_v1(ser):
    global red_raw_signal_list
    global red_moving_average_list
    global red_unfilter_signal_list
    global red_filter_signal_list

    global ir_raw_signal_list
    global ir_moving_average_list
    global ir_unfilter_signal_list
    global ir_filter_signal_list

    global ir_beat_list
    global timestamp_list

    init_flag = 0

    while True:
        if init_flag == 0:
            ser.flushInput()
            init_flag = 1
        if msvcrt.kbhit():
            key_press = msvcrt.getch().decode('ascii').lower()
            if key_press == "q": print("Stop now"); os._exit(0)
        raw_data = ser.readline().strip().decode('ascii')
        raw_data_tokens = raw_data.split(':')

        if len(raw_data_tokens) != 12: continue
        
        print(raw_data)
        
        red_raw_signal, red_moving_average, red_unfilter_signal, red_filtered_signal,\
        ir_raw_signal, ir_moving_average, ir_unfilter_signal, ir_filtered_signal,\
        beat_detected, bpm, spo2_receive, timestamp = raw_data_tokens

        red_raw_signal_list.append(int(red_raw_signal))
        red_moving_average_list.append(int(red_moving_average))
        red_unfilter_signal_list.append(int(red_unfilter_signal))
        red_filter_signal_list.append(int(red_filtered_signal))

        ir_raw_signal_list.append(int(ir_raw_signal))
        ir_moving_average_list.append(int(ir_moving_average))
        ir_unfilter_signal_list.append(int(ir_unfilter_signal))
        ir_filter_signal_list.append(int(ir_filtered_signal))

        timestamp_list.append(int(timestamp))
        ir_beat_list.append(int(beat_detected))

        if len(timestamp_list) > MAX_LIST_SIZE:
            red_raw_signal_list = red_raw_signal_list[1:]
            red_moving_average_list = red_moving_average_list[1:]
            red_unfilter_signal_list = red_unfilter_signal_list[1:]
            red_filter_signal_list = red_filter_signal_list[1:]

            ir_raw_signal_list = ir_raw_signal_list[1:]
            ir_moving_average_list = ir_moving_average_list[1:]
            ir_unfilter_signal_list = ir_unfilter_signal_list[1:]
            ir_filter_signal_list = ir_filter_signal_list[1:]

            ir_beat_list = ir_beat_list[1:]
            timestamp_list = timestamp_list[1:]

        red_signal_list[:] = [red_raw_signal_list, red_moving_average_list, red_unfilter_signal_list, red_filter_signal_list]
        ir_signal_list[:] = [ir_raw_signal_list, ir_moving_average_list, ir_unfilter_signal_list, ir_filter_signal_list, ir_beat_list]

        spo2[0] = int(spo2_receive)
        beat_per_minute[0] = int(bpm)
        #time.sleep(2e-3)

#Calculate from raw data
def get_data_v2(ser):
    global red_raw_signal_list
    global red_moving_average_list
    global red_unfilter_signal_list
    global red_filter_signal_list

    global ir_raw_signal_list
    global ir_moving_average_list
    global ir_unfilter_signal_list
    global ir_filter_signal_list

    global ir_beat_list
    global timestamp_list

    init_flag = 0
    ir_led_state = py_plotter.signal_state()
    red_led_state = py_plotter.signal_state()
    old_beat_timestamp = None
    
    while True:
        if init_flag == 0:
            ser.flushInput()
            init_flag = 1
        if msvcrt.kbhit():
            key_press = msvcrt.getch().decode('ascii').lower()
            if key_press == "q": print("Stop now"); os._exit(0)
        raw_data = ser.readline().strip().decode('ascii')
        raw_data_tokens = raw_data.split(':')

        if len(raw_data_tokens) != 12: continue
        
        print(raw_data)
        
        red_raw_signal, red_moving_average, red_unfilter_signal, red_filtered_signal,\
        ir_raw_signal, ir_moving_average, ir_unfilter_signal, ir_filtered_signal,\
        beat_detected, bpm, spo2_receive, timestamp = raw_data_tokens

        # # Bare minimum
        # raw_data_tokens = raw_data.split(';')
        # if len(raw_data_tokens) != 3: continue
        # print(raw_data)
        
        # red_raw_signal, ir_raw_signal, timestamp = raw_data_tokens

        red_led_state.raw_signal = int(red_raw_signal)
        ir_led_state.raw_signal = int(ir_raw_signal)

        red_led_state.process_signal()
        ir_led_state.process_signal()
        
        current_timestamp = int(timestamp)
        timestamp_list.append(current_timestamp)

        red_raw_signal_list.append(red_led_state.raw_signal)
        ir_raw_signal_list.append(ir_led_state.raw_signal)

        red_moving_average_list.append(red_led_state.moving_average)
        ir_moving_average_list.append(ir_led_state.moving_average)

        red_unfilter_signal_list.append(red_led_state.unfiltered_signal)
        ir_unfilter_signal_list.append(ir_led_state.unfiltered_signal)

        red_filter_signal_list.append(red_led_state.filtered_signal)
        ir_filter_signal_list.append(ir_led_state.filtered_signal)

        ir_beat_list.append(ir_led_state.checkForBeat())

        if len(timestamp_list) > MAX_LIST_SIZE:
            red_raw_signal_list = red_raw_signal_list[1:]
            red_moving_average_list = red_moving_average_list[1:]
            red_unfilter_signal_list = red_unfilter_signal_list[1:]
            red_filter_signal_list = red_filter_signal_list[1:]

            ir_raw_signal_list = ir_raw_signal_list[1:]
            ir_moving_average_list = ir_moving_average_list[1:]
            ir_unfilter_signal_list = ir_unfilter_signal_list[1:]
            ir_filter_signal_list = ir_filter_signal_list[1:]

            ir_beat_list = ir_beat_list[1:]
            timestamp_list = timestamp_list[1:]

        if old_beat_timestamp != None:
            if (current_timestamp - old_beat_timestamp >= BPM_RESET_TIME): beat_per_minute[0] = 0

        if ir_beat_list[-1]:  
            if old_beat_timestamp != None: 
                beat_per_minute[0] = int(60000. / (current_timestamp - old_beat_timestamp))
            old_beat_timestamp = current_timestamp
        
        if (ir_led_state.peak_max - ir_led_state.valley_min) > 0 and red_led_state.moving_average > 0:
            ratio_average = (red_led_state.peak_max - red_led_state.valley_min) / (ir_led_state.peak_max - ir_led_state.valley_min) * ir_led_state.moving_average / red_led_state.moving_average
            spo2[0] = math.floor(110-6*ratio_average)
            #print(str(spo2[0])+'%')
        else:
            spo2[0] = 999
        
        red_signal_list[:] = [red_raw_signal_list, red_moving_average_list, red_unfilter_signal_list, red_filter_signal_list]
        ir_signal_list[:] = [ir_raw_signal_list, ir_moving_average_list, ir_unfilter_signal_list, ir_filter_signal_list, ir_beat_list]


def animate(i, ax, timestamp_list, red_signal_list, ir_signal_list, beat_per_minute, spo2):
    if red_signal_list[0] is None or ir_signal_list[0] is None:
        return
    red_raw_signal_list, red_moving_average_list, red_unfilter_signal_list, red_filter_signal_list = red_signal_list
    ir_raw_signal_list, ir_moving_average_list, ir_unfilter_signal_list, ir_filter_signal_list, ir_beat_list = ir_signal_list

    #ax[0].clear()
    #ax[1].clear()
    for i in range(ax.shape[0]):
        for j in range(ax.shape[1]):
            ax[i,j].clear()

    # Draw
    line1, = ax[0,0].plot(timestamp_list, red_raw_signal_list, marker='.', color='blue')
    line2, = ax[0,0].plot(timestamp_list, red_moving_average_list, marker='.', color='red')

    ax[0,0].set_xlabel('Timestamp: ms')
    ax[0,0].set_ylabel('Value')
    ax[0,0].legend(['red raw value', 'red moving average'], loc='lower right')
    ax[0,0].set_title('Red raw signal and moving average')

    line3, = ax[0,1].plot(timestamp_list, ir_raw_signal_list, marker='.', color='blue')
    line4, = ax[0,1].plot(timestamp_list, ir_moving_average_list, marker='.', color='red')

    ax[0,1].set_xlabel('Timestamp: ms')
    ax[0,1].set_ylabel('Value')
    ax[0,1].legend(['ir raw value', 'ir moving average'], loc='lower right')
    ax[0,1].set_title('Ir raw signal and moving average')



    line5, = ax[1,0].plot(timestamp_list, red_unfilter_signal_list, marker='.', color='blue')

    ax[1,0].set_xlabel('Timestamp: ms')
    ax[1,0].set_ylabel('Value')
    ax[1,0].legend(['Red filtered DC signal'], loc='lower right')
    ax[1,0].set_title('Red filtered DC signal')

    line6, = ax[1,1].plot(timestamp_list, ir_unfilter_signal_list, marker='.', color='blue')
    
    ax[1,1].set_xlabel('Timestamp: ms')
    ax[1,1].set_ylabel('Value')
    ax[1,1].legend(['IR filtered DC signal'], loc='lower right')
    ax[1,1].set_title('IR filtered DC signal')



    line7, = ax[2,0].plot(timestamp_list, red_filter_signal_list, marker='.', color='blue')

    ax[2,0].set_xlabel('Timestamp: ms')
    ax[2,0].set_ylabel('Value')
    ax[2,0].legend(['Red filtered FIR+DC signal'], loc='lower right')
    ax[2,0].set_title('Red filtered FIR+DC signal')

    line8, = ax[2,1].plot(timestamp_list, ir_filter_signal_list, marker='.', color='blue')
    
    # Shift beat list left
    ir_beat_list = ir_beat_list[1:]
    ir_beat_list.append(0)

    heartbeat_timestamp = [timestamp for timestamp, beat in zip(timestamp_list, ir_beat_list) if beat == 1]
    heartbeat_ir_value = [ir_value for ir_value, beat in zip(ir_filter_signal_list, ir_beat_list) if beat == 1]

    line9, = ax[2,1].plot(heartbeat_timestamp, heartbeat_ir_value, 'ro')

    ax[2,1].set_xlabel('Timestamp: ms - BPM: ' + str(beat_per_minute[0]) + ' - SPO2: ' + str(spo2[0]) + "%")
    ax[2,1].set_ylabel('Value')
    ax[2,1].legend(['IR filtered FIR+DC signal','Heart beat'], loc='lower right')
    ax[2,1].set_title('IR filtered FIR+DC signal + Heart beat')

    return line1,line2,line3,line4,line5,line6,line7,line8,line9

def main():
    str_example =  'Example: ./py_animate.py /dev/ttyUSBx(COMx(win)) 115200'
    if len(sys.argv) != 3:
        print('Pls use it like this. ' + str_example)
        for port in serial.tools.list_ports.comports():
            print(port)
        exit(1)
    try:
        baudRate = int(sys.argv[2])
        portName = sys.argv[1]
    except ValueError:
        print('Baud rate must be int. ' + str_example)
        for port in serial.tools.list_ports.comports():
            print(port)
        exit(1)
    with serial.Serial(portName, baudRate, timeout=4) as ser:
        print("connected to: " + str(ser.portstr) + ", Baud rate: " + str(ser.baudrate))
        print("> Press q to stop")

        plot_interval = 1    # Period at which the plot animation updates [ms]

        get_data_thread = threading.Thread(target=get_data_v2, kwargs=dict(ser=ser))
        get_data_thread.start()

        fig, ax = plt.subplots(3,2)
        anim = animation.FuncAnimation(fig, animate,\
                                            fargs=(ax, timestamp_list, red_signal_list, ir_signal_list, beat_per_minute, spo2),\
                                            interval=plot_interval)
        plt.show()
 
if __name__ == '__main__':
    main()