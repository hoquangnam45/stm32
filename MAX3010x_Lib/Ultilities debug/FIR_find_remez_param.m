function [c1, c2] = FIR_find_remez_param(freq, bandwidth)
    delta = 0.001;
    init_flag = 0;
    min_dist = 999;
    fs = freq;
    a = 1;
    %warning('off','all')
    for i=delta:delta:1-2*delta
        for j=i+delta:delta:1-delta
            b = remez(22,[0 i j 1], [1 1 0 0]);
            if any(isinf(b) == 1) || any((isnan(b) == 1))
                continue
            end
            b = round(46000*b);
            [h,w] = freqz(b, a, 500, fs);
            h = h./(2^15);
            b_bandwidth = FIR_bandwidth(h, w);
            dist = abs(b_bandwidth - bandwidth);
            if init_flag == 0
                min_dist = dist;
                c1 = i;
                c2 = j;
                init_flag = 1;
                disp('>>>>>>>>')
                disp(min_dist)
                disp(b_bandwidth)
                disp(i)
                disp(j)
                disp('')
            elseif dist < min_dist
                min_dist = dist;
                c1 = i;
                c2 = j;
                disp('>>>>>>>>')
                disp(min_dist)
                disp(b_bandwidth)
                disp(i)
                disp(j)
                disp('')
            end
        end
    end
end