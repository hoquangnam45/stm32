from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
import pandas as pd
import sys 
import serial.tools.list_ports
import msvcrt
import threading
import os
import csv
import numpy as np
import math

class signal_state:
    #FIRCOEFFS = np.array([172, 321, 579, 927, 1360, 1858, 2390, 2916, 3391, 3768, 4012, 4096])
    #FIRCOEFFS = np.array([-111, -83, -10, 202, 603, 1212, 2009, 2923, 3842, 4632, 5167, 5356])
    #FIRCOEFFS = np.array([-150, -265, -376, -360, -96, 515, 1502, 2794, 4210, 5502, 6409, 6735]) # sample rate 50hz, bandwidth 3hz
    FIRCOEFFS = np.array([-112, -28, 300, 692, 613, -287, -1511, -1696, 386, 4518, 8727, 10508]) # Sample rate 25hz, bandwidth 3hz
    BUF_SIZE = 32
    NUM_BIT_BUF_SIZE = 5

    def __init__(self):
        self.raw_signal = 0

        self.input_buf = np.zeros((self.BUF_SIZE))
        self.temp_buf1 = np.zeros((self.BUF_SIZE))
        self.temp_buf2 = np.zeros((self.BUF_SIZE))
        self.moving_average_buf = np.zeros((self.BUF_SIZE))
        self.offset = 0

        self.dc_removed_signal_buf = np.zeros((32))
        self.offset_dc = 0

        self.moving_average = 0
        self.unfiltered_signal = 0
        self.filtered_signal = 0

        self.filtered_signal_pre_1 = 0 
        self.filtered_signal_pre_2 = 0
        self.maybe_peak = 0
        self.maybe_valley = 0
        self.peak_detect = 0
        self.valley_detect = 0
        self.count_monotonic = 0
        self.valley_min = 0
        self.peak_max = 0

    def process_signal(self):
        # Shift previous result
        self.filtered_signal_pre_2 = self.filtered_signal_pre_1
        self.filtered_signal_pre_1 = self.filtered_signal

        # Remove DC
        # 2-Stage Moving average DC blocker
        self.input_buf[self.offset] = self.raw_signal
        self.moving_average_buf[self.offset] = 0
        for j in range(self.BUF_SIZE):
            self.moving_average_buf[self.offset] += self.input_buf[j]
        self.moving_average_buf[self.offset] = int(self.moving_average_buf[self.offset]) >> self.NUM_BIT_BUF_SIZE

        self.moving_average = 0
        for j in range(self.BUF_SIZE):
            self.moving_average += self.moving_average_buf[j]
        self.moving_average = int(self.moving_average) >> self.NUM_BIT_BUF_SIZE
        self.unfiltered_signal = int(-self.moving_average + self.input_buf[(self.offset + 1)%self.BUF_SIZE]) #<< 2
        self.offset += 1
        self.offset %= self.BUF_SIZE


        # # Simple DC Filter
        # R = 0.95
        # self.input_buf[self.offset] = self.raw_signal
        # self.unfiltered_signal = int(self.input_buf[self.offset] - self.input_buf[int(self.offset - 1) &0x1F] + R * self.unfiltered_signal)
        # self.offset += 1
        # self.offset %= self.BUF_SIZE
        # self.moving_average = self.raw_signal - self.unfiltered_signal

        # # Sparkfun DC Filter
        # self.moving_average += int(int(int(self.raw_signal % 2^16) * 2^15 - self.moving_average) / 2^4)
        # self.unfiltered_signal = self.raw_signal - self.moving_average / 2^15

        # remove FIR
        self.filtered_signal = 0
        self.dc_removed_signal_buf[self.offset_dc] = self.unfiltered_signal
        self.filtered_signal += self.FIRCOEFFS[11] * self.dc_removed_signal_buf[(int(self.offset_dc) - 11) & 0x1F]
        
        for i in range(11):
            self.filtered_signal += self.FIRCOEFFS[i] * (self.dc_removed_signal_buf[(int(self.offset_dc) - i) & 0x1F]\
                                    + self.dc_removed_signal_buf[(int(self.offset_dc) - 22 + i) & 0x1F])

        self.filtered_signal = int(self.filtered_signal) >> 15
        self.offset_dc += 1
        self.offset_dc %= 32

        #### Fill in peak-valley info ####
        # # Reset after found peak
        if self.peak_detect == 1:
            self.maybe_peak = 0
            self.maybe_valley = 0
            self.peak_detect = 0
            self.valley_detect = 0
            self.count_monotonic = 0
    
        #dot-flat-up
        if (self.filtered_signal_pre_2  == self.filtered_signal_pre_1) and \
            (self.filtered_signal_pre_1 < self.filtered_signal):
            if self.maybe_valley==1:
                self.valley_detect = 1
                self.valley_min = self.filtered_signal_pre_1
                self.maybe_valley = 0
            if self.maybe_peak==1:
                self.maybe_peak = 0
  
        #dot-flat-down
        if (self.filtered_signal_pre_2  == self.filtered_signal_pre_1) and \
            (self.filtered_signal_pre_1 > self.filtered_signal):
            if self.maybe_valley == 1:
                self.maybe_valley = 0	
            if (self.maybe_peak==1) and (self.filtered_signal_pre_1 > 0):
                self.peak_detect = 1
                self.peak_max = self.filtered_signal_pre_1
                self.maybe_peak = 0
  
        #dot-up-flat
        if (self.filtered_signal_pre_2 < self.filtered_signal_pre_1) and \
            (self.filtered_signal_pre_1 == self.filtered_signal):
                self.maybe_peak = 1
        
        #dot-down-flat
        if ((self.filtered_signal_pre_2 > self.filtered_signal_pre_1) and \
            (self.filtered_signal_pre_1 == self.filtered_signal)):
                self.maybe_valley = 1
        
        #dot-up-down
        if (self.filtered_signal_pre_2  < self.filtered_signal_pre_1) and \
            (self.filtered_signal_pre_1 > self.filtered_signal) and (self.filtered_signal_pre_1 > 0):
            self.peak_detect = 1
            self.peak_max = self.filtered_signal_pre_1
        
        #dot-down-up
        if (self.filtered_signal_pre_2  > self.filtered_signal_pre_1) and \
            (self.filtered_signal_pre_1 < self.filtered_signal):
            self.valley_detect = 1
            self.valley_min = self.filtered_signal_pre_1   
        
        #special: detect monotonic (five-in-a-row going up)
        if self.count_monotonic < 6:
            if self.filtered_signal_pre_1 <= self.filtered_signal:
                self.count_monotonic += 1
            else:
                if self.count_monotonic > 0:
                    self.count_monotonic = 0

    def checkForBeat(self):
        beatDetected = 0
        if self.peak_detect == 1: 
            # check whether its a valid beat
            if (self.valley_detect ==1) and\
                ( self.count_monotonic == 6) and\
                ((self.peak_max -  self.valley_min) > 75) and ((self.peak_max -  self.valley_min) < 2500000):
                beatDetected = 1
        return beatDetected    

def main():
    log_file = './raw_data_sample_rate_25hz_STM32.csv' 
    red_value = []
    ir_value = []
    timestamp = []
    with open(log_file, 'r') as f:
        plots = csv.reader(f, delimiter=';')
        for row in plots:
            red_value.append(int(row[0]))
            ir_value.append(int(row[1]))
            timestamp.append(int(row[2]))

    ir_led_state = signal_state()
    red_led_state = signal_state()

    red_moving_average_list = []
    red_unfiltered_signal_list = []
    red_filtered_signal_list = []

    ir_moving_average_list = []
    ir_unfiltered_signal_list = []
    ir_filtered_signal_list = []
    ir_beat_list = []

    timestamp_beat = []
    filtered_signal_beat = []

    # remove DC
    for i in range(len(ir_value)):
        ir_led_state.raw_signal = ir_value[i]
        red_led_state.raw_signal = red_value[i]
        ir_led_state.process_signal()
        red_led_state.process_signal()
        
        ir_beat_list.append(ir_led_state.checkForBeat())
        ir_moving_average_list.append(ir_led_state.moving_average)
        ir_unfiltered_signal_list.append(ir_led_state.unfiltered_signal)
        ir_filtered_signal_list.append(ir_led_state.filtered_signal)

        red_moving_average_list.append(red_led_state.moving_average)
        red_unfiltered_signal_list.append(red_led_state.unfiltered_signal)
        red_filtered_signal_list.append(red_led_state.filtered_signal)

    fig, ax = plt.subplots(4,1)

    ax[0].plot(timestamp, ir_value, marker='.', color='black')
    ax[0].plot(timestamp, ir_moving_average_list, marker='.', color='blue')
    
    ax[1].plot(timestamp, red_value, marker='.', color='red')
    ax[1].plot(timestamp, red_moving_average_list, marker='.', color='green')

    ax[2].plot(timestamp, ir_unfiltered_signal_list, marker='.', color='black')
    ax[2].plot(timestamp, ir_filtered_signal_list, marker='.', color='cyan')

    for i in range(len(ir_beat_list)):
        if ir_beat_list[i] == 1:
            timestamp_beat.append(timestamp[i-1])
            filtered_signal_beat.append(ir_filtered_signal_list[i-1])
    
    ax[2].plot(timestamp_beat, filtered_signal_beat, 'ro')

    ax[3].plot(timestamp, red_unfiltered_signal_list, marker='.', color='red')
    ax[3].plot(timestamp, red_filtered_signal_list, marker='.', color='cyan')

    ax[0].set_xlabel('Timestamp: ms')
    ax[0].legend(['ir value', 'ir moving average'], loc='lower right')
    ax[0].set_ylabel('Value')

    ax[1].set_xlabel('Timestamp: ms')
    ax[1].legend(['red value', 'red moving average'], loc='lower right')
    ax[1].set_ylabel('Value')

    ax[2].set_xlabel('Timestamp: ms')
    ax[2].legend(['ir removed DC value', 'ir FIR filtered signal', 'Hear beat detect'], loc='lower right')
    ax[2].set_ylabel('IR Value')

    ax[3].set_xlabel('Timestamp: ms')
    ax[3].legend(['red removed DC value', 'red FIR filtered signal'], loc='lower right')
    ax[3].set_ylabel('Value')

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()
 
if __name__ == '__main__':
    main()