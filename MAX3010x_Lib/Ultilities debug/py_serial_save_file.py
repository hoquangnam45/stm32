from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
import pandas as pd
import sys 
import serial.tools.list_ports
import msvcrt
import threading
import os

def main():
    str_example =  'Example: ./py_serial_save_file.py /dev/ttyUSBx(COMx(win)) 115200'
    if len(sys.argv) != 3:
        print('Pls use it like this. ' + str_example)
        for port in serial.tools.list_ports.comports():
            print(port)
        exit(1)
    try:
        baudRate = int(sys.argv[2])
        portName = sys.argv[1]
    except ValueError:
        print('Baud rate must be int. ' + str_example)
        for port in serial.tools.list_ports.comports():
            print(port)
        exit(1)
    with serial.Serial(portName, baudRate, timeout=4) as ser:
        print("connected to: " + str(ser.portstr) + ", Baud rate: " + str(ser.baudrate))
        print("> Press q to stop")
        log_file = './raw_data.csv'
        init_flag = 0
        with open(log_file, 'w') as f:
            while True:
                if init_flag == 0:
                    ser.flushInput()
                    init_flag = 1
                if msvcrt.kbhit():
                    key_press = msvcrt.getch().decode('ascii').lower()
                    if key_press == "q": print("Stop now"); os._exit(0)
                raw_data = ser.readline().strip().decode('ascii')
                timestamp = round(time.perf_counter() * 1000)
                print(str(timestamp) + '> ' + raw_data)
                f.write(raw_data+'\n')
 
if __name__ == '__main__':
    main()