/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern SPI_HandleTypeDef hspi2;
extern UART_HandleTypeDef huart1;
/* USER CODE BEGIN EV */
extern const int BUF_SIZE; 
extern char SPI_buf[];
extern int receiveDone;
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles SPI2 global interrupt.
  */
//void SPI_DMAAbortOnError(DMA_HandleTypeDef *hdma);
void SPI2_IRQHandler(void)
{
	//__HAL_SPI_DISABLE_IT(&hspi2, (SPI_IT_RXNE | SPI_IT_ERR));
	static int idx = 0;
	uint32_t itsource = hspi2.Instance->CR2;
  uint32_t itflag   = hspi2.Instance->SR;

  /* SPI in mode Receiver ----------------------------------------------------*/
  if ((SPI_CHECK_FLAG(itflag, SPI_FLAG_OVR) == RESET) &&
      (SPI_CHECK_FLAG(itflag, SPI_FLAG_RXNE) != RESET) && (SPI_CHECK_IT_SOURCE(itsource, SPI_IT_RXNE) != RESET))
  {
		if (idx < BUF_SIZE - 2){
			char rxchar = hspi2.Instance->DR;
			if (rxchar != '\n'){
				SPI_buf[idx++] = rxchar;
			}
			else{
				SPI_buf[idx++] = '\n';
				SPI_buf[idx] = 0;
				idx = 0;
				receiveDone = 1;
				__HAL_SPI_DISABLE_IT(&hspi2, (SPI_IT_RXNE | SPI_IT_ERR));
			}
		}
  }
	//__HAL_SPI_ENABLE_IT(&hspi2, (SPI_IT_RXNE | SPI_IT_ERR));
	return;

//  /* SPI in mode Transmitter -------------------------------------------------*/
//  if ((SPI_CHECK_FLAG(itflag, SPI_FLAG_TXE) != RESET) && (SPI_CHECK_IT_SOURCE(itsource, SPI_IT_TXE) != RESET))
//  {
//    hspi2.TxISR(&hspi2);
//    return;
//  }

//  /* SPI in Error Treatment --------------------------------------------------*/
//  if (((SPI_CHECK_FLAG(itflag, SPI_FLAG_MODF) != RESET) || (SPI_CHECK_FLAG(itflag, SPI_FLAG_OVR) != RESET))
//       && (SPI_CHECK_IT_SOURCE(itsource, SPI_IT_ERR) != RESET))
//  {
//    /* SPI Overrun error interrupt occurred ----------------------------------*/
//    if (SPI_CHECK_FLAG(itflag, SPI_FLAG_OVR) != RESET)
//    {
//      if (hspi2.State != HAL_SPI_STATE_BUSY_TX)
//      {
//        SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_OVR);
//        __HAL_SPI_CLEAR_OVRFLAG(&hspi2);
//      }
//      else
//      {
//        __HAL_SPI_CLEAR_OVRFLAG(&hspi2);
//        return;
//      }
//    }

//    /* SPI Mode Fault error interrupt occurred -------------------------------*/
//    if (SPI_CHECK_FLAG(itflag, SPI_FLAG_MODF) != RESET)
//    {
//      SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_MODF);
//      __HAL_SPI_CLEAR_MODFFLAG(&hspi2);
//    }

//    /* SPI Frame error interrupt occurred ------------------------------------*/

//    if (hspi2.ErrorCode != HAL_SPI_ERROR_NONE)
//    {
//      /* Disable all interrupts */
//      __HAL_SPI_DISABLE_IT(&hspi2, SPI_IT_RXNE | SPI_IT_TXE | SPI_IT_ERR);

//      hspi2.State = HAL_SPI_STATE_READY;
//      /* Disable the SPI DMA requests if enabled */
//      if ((HAL_IS_BIT_SET(itsource, SPI_CR2_TXDMAEN)) || (HAL_IS_BIT_SET(itsource, SPI_CR2_RXDMAEN)))
//      {
//        CLEAR_BIT(hspi2.Instance->CR2, (SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN));

////        /* Abort the SPI DMA Rx channel */
////        if (hspi2.hdmarx != NULL)
////        {
////          /* Set the SPI DMA Abort callback :
////          will lead to call HAL_SPI_ErrorCallback() at end of DMA abort procedure */
////          hspi2.hdmarx->XferAbortCallback = SPI_DMAAbortOnError;
////          if (HAL_OK != HAL_DMA_Abort_IT(hspi2.hdmarx))
////          {
////            SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_ABORT);
////          }
////        }
//        /* Abort the SPI DMA Tx channel */
////        if (hspi2.hdmatx != NULL)
////        {
////          /* Set the SPI DMA Abort callback :
////          will lead to call HAL_SPI_ErrorCallback() at end of DMA abort procedure */
////          hspi2.hdmatx->XferAbortCallback = SPI_DMAAbortOnError;
////          if (HAL_OK != HAL_DMA_Abort_IT(hspi2.hdmatx))
////          {
////            SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_ABORT);
////          }
////        }
//      }
//      else
//      {
//        /* Call user error callback */
//#if (USE_HAL_SPI_REGISTER_CALLBACKS == 1U)
//        hspi->ErrorCallback(hspi);
//#else
//        HAL_SPI_ErrorCallback(&hspi2);
//#endif /* USE_HAL_SPI_REGISTER_CALLBACKS */
//      }
//    }
//    return;
//  }
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */

  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */

  /* USER CODE END USART1_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
